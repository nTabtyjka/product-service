package com.epam.sberdex.product.repository;

import com.epam.sberdex.product.entity.SampleEntity;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface SampleRepository extends CrudRepository<SampleEntity, UUID> {}
