package com.epam.sberdex.product.service.camunda.impl;

import static java.util.stream.Collectors.toMap;

import com.epam.sberdex.camundalib.api.CustomTaskApi;
import com.epam.sberdex.camundalib.model.SearchCurrentTaskDto;
import com.epam.sberdex.product.model.CamundaVariableDto;
import com.epam.sberdex.product.model.CompleteUserTaskDto;
import com.epam.sberdex.product.service.camunda.CamundaService;
import com.epam.sberdex.product.util.constants.CamundaConstant;
import io.swagger.client.ApiException;
import io.swagger.client.api.ProcessDefinitionApi;
import io.swagger.client.api.ProcessInstanceApi;
import io.swagger.client.model.CompleteTaskDto;
import io.swagger.client.model.ProcessInstanceDto;
import io.swagger.client.model.StartProcessInstanceDto;
import io.swagger.client.model.TaskDto;
import io.swagger.client.model.VariableValueDto;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class CamundaServiceImpl implements CamundaService {
  private final ProcessDefinitionApi processDefinitionApi;
  private final ProcessInstanceApi processInstanceApi;
  private final CustomTaskApi taskApi;

  /**
   * StartBusiesProcess.
   *
   * @param processKey process definition key
   * @return uuid instance.
   * @throws ApiException Exception.
   */
  @Override
  public UUID startBusinessProcess(String processKey) throws ApiException {
    ProcessInstanceDto processInstanceDto =
        processDefinitionApi.startProcessInstance(processKey, new StartProcessInstanceDto());
    return UUID.fromString(processInstanceDto.getId());
  }

  /**
   * DeleteBusinessProcess.
   *
   * @param processInstanceId processInstanceId
   * @throws ApiException Exception.
   */
  @Override
  public void deleteBusinessProcess(UUID processInstanceId) throws ApiException {
    processInstanceApi.deleteProcessInstance(processInstanceId.toString(), true, true, true);
  }

  /**
   * SetVariableToInstance.
   *
   * @param instanceId uuid instance.
   * @param varName variable name.
   * @param variableValue variable value.
   * @throws ApiException Exception.
   */
  @Override
  public void setVariableToInstance(UUID instanceId, String varName, VariableValueDto variableValue)
      throws ApiException {
    processInstanceApi.putVariableByInstanceIdAndVariablesId(
        varName, instanceId.toString(), variableValue);
  }

  /**
   * UpdateVariable.
   *
   * @param instanceId uuid instance.
   * @param varName variable name.
   * @param variableValue variable value.
   * @throws ApiException Exception.
   */
  @Override
  public void updateVariable(UUID instanceId, String varName, VariableValueDto variableValue)
      throws ApiException {
    setVariableToInstance(instanceId, varName, variableValue);
  }

  /**
   * CancelCurrentUserTask.
   *
   * @param instanceId uuid instance.
   * @throws ApiException Exception.
   */
  @Override
  public void cancelCurrentUserTask(UUID instanceId) throws ApiException {
    taskApi.complete(getTaskId(instanceId), new CompleteTaskDto());
  }

  /**
   * CancelCurrentUserTask.
   *
   * @param instanceId uuid instance.
   * @param dto complete task.
   * @throws ApiException Exception.
   */
  @Override
  public void cancelCurrentUserTask(UUID instanceId, CompleteUserTaskDto dto) throws ApiException {
    Map<String, VariableValueDto> variables =
        dto.getParams().stream()
            .collect(toMap(CamundaVariableDto::getName, this::getVariableValueDto));
    if (StringUtils.hasText(dto.getMessage())) {
      variables.put(
          CamundaConstant.VAR_NAME_MESSAGE,
          new VariableValueDto().value(dto.getMessage()).type(CamundaConstant.VAR_TYPE_STRING));
    }
    taskApi.complete(getTaskId(instanceId), new CompleteTaskDto().variables(variables));
  }

  private VariableValueDto getVariableValueDto(CamundaVariableDto variableDto) {
    VariableValueDto valueDto = new VariableValueDto();
    valueDto.setType(variableDto.getType());
    valueDto.setValue(variableDto.getValue());
    return valueDto;
  }

  /**
   * GetTaskId.
   *
   * @param instanceId uuid instance.
   * @return task id.
   * @throws ApiException api exception.
   */
  private String getTaskId(UUID instanceId) throws ApiException {
    List<TaskDto> currentTasks = taskApi.getTask(new SearchCurrentTaskDto(instanceId.toString()));
    return currentTasks
        .get(0) // 0 элемент т.к. по id инстанса, текущей задачей может быть только одна;
        .getId();
  }
}
