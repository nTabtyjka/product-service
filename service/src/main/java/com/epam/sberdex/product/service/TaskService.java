package com.epam.sberdex.product.service;

import com.epam.sberdex.product.model.CompleteUserTaskDto;
import java.util.UUID;
import javassist.NotFoundException;

public interface TaskService {
  void completeTask(UUID id, CompleteUserTaskDto dto) throws NotFoundException;
}
