package com.epam.sberdex.product.service.rest;

import com.epam.sberdex.product.api.TaskApi;
import com.epam.sberdex.product.model.CompleteUserTaskDto;
import com.epam.sberdex.product.service.TaskService;
import java.util.UUID;
import javassist.NotFoundException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class TaskApiImpl implements TaskApi {

  private final TaskService service;

  @Override
  public ResponseEntity<Void> completeTask(
      UUID id, @Valid CompleteUserTaskDto completeUserTaskDto) {
    try {
      service.completeTask(id, completeUserTaskDto);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }
}
