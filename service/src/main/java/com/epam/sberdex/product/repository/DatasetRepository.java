package com.epam.sberdex.product.repository;

import com.epam.sberdex.product.entity.DatasetEntity;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatasetRepository extends CrudRepository<DatasetEntity, UUID> {}
