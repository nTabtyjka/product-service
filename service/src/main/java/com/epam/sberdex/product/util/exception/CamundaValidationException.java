package com.epam.sberdex.product.util.exception;

public class CamundaValidationException extends Exception {

  public CamundaValidationException(String message) {
    super(message);
  }
}
