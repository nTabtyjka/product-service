package com.epam.sberdex.product.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileTransferHelper {

  @Value("${spring.application.upload-file-dir}")
  private String uploadFileDir = "/tmp";

  public Path createDirectory(Path resolvedPath) throws IOException {
    return Files.createDirectories(resolvedPath);
  }

  public void multipartFileTransfer(MultipartFile file, Path resolvedPath) throws IOException {
    file.transferTo(resolvedPath);
  }

  public Resource getResource(Path resolvedPath) {
    return new FileSystemResource(resolvedPath);
  }

  public void delete(Path resolvedPath) throws IOException {
    Files.delete(resolvedPath);
  }

  public Path resolvePath(Path path) {
    return Paths.get(uploadFileDir).resolve(path);
  }
}
