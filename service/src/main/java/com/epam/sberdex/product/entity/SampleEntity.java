package com.epam.sberdex.product.entity;

import com.epam.sberdex.common.db.entity.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "sample")
public class SampleEntity extends BaseEntity {

  private String name;

  @Column(name = "mime_type", length = 64)
  private String mimeType;

  private Long size;

  @ManyToOne
  @JoinColumn(name = "product_id")
  private ProductEntity productEntity;
}
