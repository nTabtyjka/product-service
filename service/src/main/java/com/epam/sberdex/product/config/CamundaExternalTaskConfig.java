package com.epam.sberdex.product.config;

import com.epam.sberdex.product.service.camunda.handlers.ChangeStatusHandler;
import com.epam.sberdex.product.util.constants.CamundaConstant;
import org.camunda.bpm.client.ExternalTaskClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CamundaExternalTaskConfig {
  @Autowired private ChangeStatusHandler changeStatusHandler;

  @Value("${spring.application.services.camunda.path}")
  private String camundaPath;

  /**
   * Been Camunda ExternalTaskClient.
   *
   * @return ExternalTaskClient.
   */
  @Bean
  public ExternalTaskClient externalTaskClient() {
    ExternalTaskClient client = ExternalTaskClient.create().baseUrl(camundaPath).build();
    client
        .subscribe(CamundaConstant.EXTERNAL_TASK_TOPIC_CHANGE_STATUS)
        .handler(changeStatusHandler)
        .open();
    return client;
  }
}
