package com.epam.sberdex.product.repository;

import com.epam.sberdex.product.entity.FileEntity;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository<FileEntity, UUID> {}
