package com.epam.sberdex.product.service.impl;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.mapper.ProductMapper;
import com.epam.sberdex.product.model.Pagination;
import com.epam.sberdex.product.model.Product;
import com.epam.sberdex.product.model.Product.StatusEnum;
import com.epam.sberdex.product.model.Product.TypeEnum;
import com.epam.sberdex.product.model.ProductPage;
import com.epam.sberdex.product.repository.ProductRepository;
import com.epam.sberdex.product.service.DatasetService;
import com.epam.sberdex.product.service.ProductService;
import com.epam.sberdex.product.service.camunda.CamundaService;
import com.epam.sberdex.product.util.constants.CamundaConstant;
import io.swagger.client.ApiException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final DatasetService datasetService;
  private final CamundaService camundaService;
  private final ProductRepository productRepository;
  private final ProductMapper mapper;

  @Override
  public ProductPage searchProducts(Integer pageNum, Integer pageSize, String status) {
    Page<ProductEntity> pageProd;
    PageRequest pageRequest =
        PageRequest.of(
            pageNum,
            pageSize,
            new Sort(Direction.ASC, ProductEntity.FIELD_NAME, ProductEntity.FIELD_ID));
    if (StringUtils.hasText(status)) {
      if (!checkAvailableStatuses(status)) {
        throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Wrong status!");
      }
      pageProd = productRepository.findByStatus(pageRequest, Product.StatusEnum.valueOf(status));
    } else {
      pageProd = productRepository.findAll(pageRequest);
    }
    final ProductPage productPage = new ProductPage();
    productPage
        .content(mapper.toDtoProducts(pageProd.stream().collect(Collectors.toList())))
        .total(pageProd.getTotalElements())
        .pagination(
            new Pagination()
                .pageCount(pageProd.getTotalPages())
                .pageNum(pageProd.getNumber())
                .pageSize(pageProd.getSize()));
    return productPage;
  }

  @Override
  public Product addProduct(Product product) {
    UUID instanceId;
    try {
      instanceId = camundaService.startBusinessProcess(CamundaConstant.PROCESS_KEY);
    } catch (ApiException e) {
      log.error(e.getMessage(), e);
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Can't start process");
    }
    final ProductEntity entity = mapper.toEntityProduct(product);
    entity.setTenantId(UUID.randomUUID().toString()); // multitenancy will be implemented later
    entity.setStatus(StatusEnum.DRAFT);
    entity.setInstanceId(instanceId);
    final ProductEntity saved = productRepository.save(entity);
    if (product.getType().equals(TypeEnum.ONE_TIME)) {
      datasetService.dataSetAutoCreate(saved);
    }
    return mapper.toDtoProduct(saved);
  }

  @Override
  public Product getProduct(UUID id) throws NotFoundException {
    final Optional<ProductEntity> prod = productRepository.findById(id);
    if (prod.isPresent()) {
      return mapper.toDtoProduct(prod.get());
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  @Override
  public void updateProduct(UUID id, Product product) throws NotFoundException {
    if (productRepository.existsById(id)) {
      final ProductEntity entity = productRepository.findById(id).orElse(null);
      if (entity != null) {
        final ProductEntity mappedEntity = mapper.updateFromDto(product, entity);
        if (mappedEntity.getStatus() == StatusEnum.REJECTED) {
          mappedEntity.setStatus(StatusEnum.DRAFT);
        }
        productRepository.save(mappedEntity);
      }
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  @Override
  public void deleteProduct(UUID id) throws NotFoundException {
    ProductEntity productEntity = productRepository.findById(id).orElse(null);
    if (productEntity != null) {
      final UUID instanceId = productEntity.getInstanceId();
      productRepository.deleteById(id);
      try {
        camundaService.deleteBusinessProcess(instanceId);
      } catch (ApiException e) {
        log.error(e.getMessage(), e);
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Can't delete process");
      }
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  private boolean checkAvailableStatuses(String status) {
    for (Product.StatusEnum statusEnum : Product.StatusEnum.values()) {
      if (statusEnum.name().equals(status)) {
        return true;
      }
    }
    return false;
  }
}
