package com.epam.sberdex.product.service.camunda;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.util.exception.CamundaValidationException;
import javax.validation.Valid;

public interface CamundaValidationService {

  boolean validate(@Valid ProductEntity productEntity) throws CamundaValidationException;

  boolean validateDatasetFileHash(@Valid ProductEntity productEntity)
      throws CamundaValidationException;
}
