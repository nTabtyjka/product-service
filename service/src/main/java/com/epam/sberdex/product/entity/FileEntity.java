package com.epam.sberdex.product.entity;

import com.epam.sberdex.common.db.entity.BaseEntity;
import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "file")
public class FileEntity extends BaseEntity {

  private String name;

  @Column(length = 512)
  @Pattern(regexp = "[0-9a-fA-F]{32}")
  private String hash;

  @Column(length = 128)
  private String mimeType;

  @Column(length = 36)
  private String contentId;

  private Long size;

  private OffsetDateTime uploadTime;

  private OffsetDateTime moveToStorageTime;

  @ManyToOne
  @JoinColumn(name = "dataset_id")
  private DatasetEntity datasetEntity;
}
