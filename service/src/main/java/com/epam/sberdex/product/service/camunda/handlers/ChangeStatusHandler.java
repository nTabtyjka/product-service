package com.epam.sberdex.product.service.camunda.handlers;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.model.Product.StatusEnum;
import com.epam.sberdex.product.repository.ProductRepository;
import com.epam.sberdex.product.service.camunda.CamundaValidationService;
import com.epam.sberdex.product.util.constants.CamundaConstant;
import com.epam.sberdex.product.util.exception.CamundaValidationException;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class ChangeStatusHandler implements ExternalTaskHandler {
  @Autowired private CamundaValidationService service;
  @Autowired private ProductRepository repository;

  @Override
  @Transactional
  public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
    ProductEntity product =
        repository.findByInstanceId(UUID.fromString(externalTask.getProcessInstanceId()));
    try {
      if (service.validate(product)) {
        StatusEnum status =
            StatusEnum.fromValue(externalTask.getVariable(CamundaConstant.VAR_NAME_STATUS));
        try {
          statusSelector(product, status, externalTask);
        } catch (CamundaValidationException e) {
          externalTaskService.handleBpmnError(externalTask, "Validation hashes failed");
          throw e;
        }
        product.setStatus(status);
        repository.save(product);
        externalTaskService.complete(externalTask);
      }
    } catch (CamundaValidationException e) {
      log.error(e.getMessage(), e);
    }
  }

  private void statusSelector(
      ProductEntity productEntity, StatusEnum status, ExternalTask externalTask)
      throws CamundaValidationException {
    if (StatusEnum.REJECTED.equals(status)) {
      String rejectionReason = externalTask.getVariable(CamundaConstant.VAR_NAME_MESSAGE);
      productEntity.setRejectionReason(rejectionReason);
    } else if (status == StatusEnum.APPROVED) {
      service.validateDatasetFileHash(productEntity);
    }
    if (StatusEnum.PUBLISHED.equals(status)) {
      productEntity.setRejectionReason("");
    }
  }
}
