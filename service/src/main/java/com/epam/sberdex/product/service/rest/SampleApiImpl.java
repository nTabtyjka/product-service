package com.epam.sberdex.product.service.rest;

import com.epam.sberdex.product.api.SampleApi;
import com.epam.sberdex.product.model.Sample;
import com.epam.sberdex.product.service.SampleService;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequiredArgsConstructor
public class SampleApiImpl implements SampleApi {

  private final SampleService service;

  @Override
  public ResponseEntity<List<Sample>> addSample(UUID productId, MultipartFile file) {
    try {
      return ResponseEntity.ok(service.addSample(productId, file));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Void> deleteSample(UUID productId, UUID sampleId) {
    try {
      service.deleteSample(productId, sampleId);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Resource> downloadSample(UUID productId, UUID sampleId) {
    try {
      Resource resource = service.downloadSample(productId, sampleId);
      final HttpHeaders headers = new HttpHeaders();
      final ContentDisposition contentDisposition =
          ContentDisposition.builder("attachment")
              .filename(resource.getFile().getName())
              .size(resource.getFile().length())
              .build();
      headers.setContentDisposition(contentDisposition);
      return ResponseEntity.status(HttpStatus.OK).headers(headers).body(resource);
    } catch (NotFoundException | IOException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<List<Sample>> getSamples(UUID productId) {
    try {
      return ResponseEntity.ok(service.getSamples(productId));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }
}
