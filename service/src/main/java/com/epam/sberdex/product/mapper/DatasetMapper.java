package com.epam.sberdex.product.mapper;

import com.epam.sberdex.product.entity.DatasetEntity;
import com.epam.sberdex.product.model.Dataset;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface DatasetMapper {
  Dataset toDtoDataset(DatasetEntity datasetEntity);

  DatasetEntity toEntity(Dataset dto);

  List<Dataset> toListDto(List<DatasetEntity> datasetEntities);

  @Mapping(target = "id", ignore = true)
  DatasetEntity updateDatasetFromDto(@MappingTarget DatasetEntity entity, Dataset dto);
}
