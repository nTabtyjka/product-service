package com.epam.sberdex.product.util.constants;

public class CamundaConstant {
  public static final String ID_KEY = "id";
  public static final String PROCESS_KEY = "Process_0zz2cu3";
  public static final String EXTERNAL_TASK_TOPIC_CHANGE_STATUS = "changeStatus";
  public static final String VAR_NAME_STATUS = "status";
  public static final String VAR_NAME_MESSAGE = "message";
  public static final String VAR_TYPE_STRING = "string";
  public static final String CAMUNDA_DATE_TIME_PATTERN = "yyyy-MM-dd['T'HH:mm[:ss[.SSS[Z]]]]";
}
