package com.epam.sberdex.product.service;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.model.Dataset;
import com.epam.sberdex.product.model.FileDto;
import com.epam.sberdex.product.model.FileHashDto;
import java.util.List;
import java.util.UUID;
import javassist.NotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface DatasetService {
  void dataSetAutoCreate(ProductEntity entity);

  void addFileHash(UUID id, UUID fileId, FileHashDto fileHashDto) throws NotFoundException;

  FileDto addUploadFile(UUID id, MultipartFile file) throws NotFoundException;

  void deleteDataset(UUID id) throws NotFoundException;

  FileDto addFile(UUID id, FileDto fileDto) throws NotFoundException;

  void deleteFile(UUID id, UUID fileId) throws NotFoundException;

  Resource downloadFile(UUID id, UUID fileId) throws NotFoundException;

  List<FileDto> getFiles(UUID id) throws NotFoundException;

  void updateFile(UUID id, UUID fileId, FileDto fileDto) throws NotFoundException;

  void uploadFile(UUID id, UUID fileId, MultipartFile file) throws NotFoundException;

  Dataset getDataset(UUID id) throws NotFoundException;

  void updateDataset(UUID id, Dataset dataset) throws NotFoundException;

  Dataset addProductDataset(UUID id, Dataset dataset) throws NotFoundException;

  List<Dataset> getProductDatasets(UUID id) throws NotFoundException;
}
