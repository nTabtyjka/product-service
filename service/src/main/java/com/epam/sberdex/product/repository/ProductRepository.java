package com.epam.sberdex.product.repository;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.model.Product;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, UUID> {
  Page<ProductEntity> findAll(Pageable pageable);

  Page<ProductEntity> findByStatus(Pageable pageable, Product.StatusEnum status);

  ProductEntity findByInstanceId(UUID instanceId);
}
