package com.epam.sberdex.product.mapper;

import com.epam.sberdex.product.entity.SampleEntity;
import com.epam.sberdex.product.model.Sample;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface SampleMapper {

  Sample toDtoSample(SampleEntity entity);

  SampleEntity toEntitySample(Sample productDto);

  List<Sample> toDtoSamples(List<SampleEntity> entities);

  @Mapping(target = "id", ignore = true)
  SampleEntity updateFromDto(Sample sampleDto, @MappingTarget SampleEntity entity);
}
