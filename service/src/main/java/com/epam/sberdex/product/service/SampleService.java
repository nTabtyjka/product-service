package com.epam.sberdex.product.service;

import com.epam.sberdex.product.model.Sample;
import java.util.List;
import java.util.UUID;
import javassist.NotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface SampleService {
  List<Sample> addSample(UUID productId, MultipartFile file) throws NotFoundException;

  List<Sample> getSamples(UUID productId) throws NotFoundException;

  void deleteSample(UUID productId, UUID sampleId) throws NotFoundException;

  Resource downloadSample(UUID productId, UUID sampleId) throws NotFoundException;
}
