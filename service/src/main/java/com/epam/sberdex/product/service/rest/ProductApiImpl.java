package com.epam.sberdex.product.service.rest;

import com.epam.sberdex.product.api.ProductApi;
import com.epam.sberdex.product.model.Product;
import com.epam.sberdex.product.model.ProductPage;
import com.epam.sberdex.product.service.ProductService;
import java.util.UUID;
import javassist.NotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ProductApiImpl implements ProductApi {

  private final ProductService service;

  @Override
  public ResponseEntity<Product> addProduct(@Valid Product product) {
    return ResponseEntity.ok(service.addProduct(product));
  }

  @Override
  public ResponseEntity<Void> deleteProduct(UUID id) {
    try {
      service.deleteProduct(id);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Product> getProduct(UUID id) {
    try {
      return ResponseEntity.ok(service.getProduct(id));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<ProductPage> searchProducts(
      @Min(0) @Valid Integer pageNum,
      @Min(1) @Max(50) @Valid Integer pageSize,
      @Valid String status) {
    return ResponseEntity.ok(service.searchProducts(pageNum, pageSize, status));
  }

  @Override
  public ResponseEntity<Void> updateProduct(UUID id, @Valid Product product) {
    try {
      service.updateProduct(id, product);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }
}
