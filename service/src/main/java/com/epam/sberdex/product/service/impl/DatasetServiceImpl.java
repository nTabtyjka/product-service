package com.epam.sberdex.product.service.impl;

import com.epam.sberdex.product.entity.DatasetEntity;
import com.epam.sberdex.product.entity.FileEntity;
import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.mapper.DatasetMapper;
import com.epam.sberdex.product.mapper.FileMapper;
import com.epam.sberdex.product.model.Dataset;
import com.epam.sberdex.product.model.Dataset.DeliveryTypeEnum;
import com.epam.sberdex.product.model.FileDto;
import com.epam.sberdex.product.model.FileHashDto;
import com.epam.sberdex.product.repository.DatasetRepository;
import com.epam.sberdex.product.repository.FileRepository;
import com.epam.sberdex.product.repository.ProductRepository;
import com.epam.sberdex.product.service.DatasetService;
import com.epam.sberdex.product.service.FileTransferService;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class DatasetServiceImpl implements DatasetService {
  private final ProductRepository productRepository;
  private final DatasetRepository datasetRepository;
  private final FileRepository fileRepository;
  private final FileTransferService transferService;
  private final FileMapper fileMapper;
  private final DatasetMapper datasetMapper;

  @Value("${spring.application.dataset-root}")
  private String datasetRoot = "";

  @Override
  public void dataSetAutoCreate(ProductEntity entity) {
    DatasetEntity datasetEntity = new DatasetEntity();
    datasetEntity.setName(entity.getName() + " - " + entity.getCreatedAt());
    datasetEntity.setStatus(Dataset.StatusEnum.DRAFT);
    datasetEntity.setDeliveryType(DeliveryTypeEnum.PUSH);
    datasetEntity.setProductEntity(entity);
    datasetRepository.save(datasetEntity);
  }

  @Override
  public void addFileHash(UUID id, UUID fileId, FileHashDto fileHashDto) throws NotFoundException {
    final Optional<DatasetEntity> dataset = datasetRepository.findById(id);
    if (dataset.isPresent()) {
      final Optional<FileEntity> fileEntity = fileRepository.findById(fileId);
      if (fileEntity.isPresent()) {
        fileEntity.get().setHash(fileHashDto.getHash());
        fileRepository.save(fileEntity.get());
      } else {
        throw new NotFoundException("File not found");
      }
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public FileDto addUploadFile(UUID id, MultipartFile file) throws NotFoundException {
    final Optional<DatasetEntity> dataset = datasetRepository.findById(id);
    if (dataset.isPresent()) {
      dataset.get().getProductEntity().setLastUploadAt(OffsetDateTime.now());
      FileEntity entity = new FileEntity();
      entity.setSize(file.getSize());
      entity.setMimeType(file.getContentType());
      entity.setUploadTime(OffsetDateTime.now());
      entity.setName(file.getOriginalFilename());
      entity.setDatasetEntity(dataset.get());
      entity = fileRepository.save(entity);
      Path path =
          Paths.get(datasetRoot, id.toString(), entity.getId() + "-" + file.getOriginalFilename());
      transferService.upload(path, file);
      return fileMapper.toDtoFile(entity);
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public void deleteDataset(UUID id) throws NotFoundException {
    if (datasetRepository.existsById(id)) {
      datasetRepository.deleteById(id);
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public FileDto addFile(UUID id, FileDto fileDto) throws NotFoundException {
    final Optional<DatasetEntity> dataset = datasetRepository.findById(id);
    if (dataset.isPresent()) {
      FileEntity fileEntity = fileMapper.toEntityFile(fileDto);
      fileEntity.setDatasetEntity(dataset.get());
      fileEntity = fileRepository.save(fileEntity);
      return fileMapper.toDtoFile(fileEntity);
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public void deleteFile(UUID id, UUID fileId) throws NotFoundException {
    final Optional<DatasetEntity> dataset = datasetRepository.findById(id);
    if (dataset.isPresent()) {
      final Optional<FileEntity> fileEntity = fileRepository.findById(fileId);
      if (fileEntity.isPresent()) {
        Path path =
            Paths.get(datasetRoot, id.toString(), fileId + "-" + fileEntity.get().getName());
        transferService.delete(path);
        fileRepository.deleteById(fileId);
      } else {
        throw new NotFoundException("File not found");
      }
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public Resource downloadFile(UUID id, UUID fileId) throws NotFoundException {
    if (datasetRepository.existsById(id)) {
      Optional<FileEntity> fileEntity = fileRepository.findById(fileId);
      if (fileEntity.isPresent()) {
        Path path =
            Paths.get(datasetRoot, id.toString(), fileId + "-" + fileEntity.get().getName());
        return transferService.download(path);
      } else {
        throw new NotFoundException("File not found");
      }
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public List<FileDto> getFiles(UUID id) throws NotFoundException {
    final Optional<DatasetEntity> dataset = datasetRepository.findById(id);
    if (dataset.isPresent()) {
      return fileMapper.toDtoFiles(dataset.get().getFileEntities());
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public void updateFile(UUID id, UUID fileId, FileDto fileDto) throws NotFoundException {
    final Optional<DatasetEntity> dataset = datasetRepository.findById(id);
    if (dataset.isPresent()) {
      final Optional<FileEntity> fileEntity = fileRepository.findById(fileId);
      if (fileEntity.isPresent()) {
        FileEntity entity = fileMapper.updateFromDto(fileDto, fileEntity.get());
        fileRepository.save(entity);
      } else {
        throw new NotFoundException("File not found");
      }
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public void uploadFile(UUID id, UUID fileId, MultipartFile file) throws NotFoundException {
    final Optional<DatasetEntity> dataset = datasetRepository.findById(id);
    if (dataset.isPresent()) {
      final Optional<FileEntity> fileEntity = fileRepository.findById(fileId);
      dataset.get().getProductEntity().setLastUploadAt(OffsetDateTime.now());
      if (fileEntity.isPresent()) {
        final FileEntity entity = fileEntity.get();
        entity.setSize(file.getSize());
        entity.setMimeType(file.getContentType());
        entity.setUploadTime(OffsetDateTime.now());
        entity.setName(file.getOriginalFilename());
        Path path =
            Paths.get(datasetRoot, id.toString(), fileId + "-" + file.getOriginalFilename());
        transferService.upload(path, file);
        fileRepository.save(entity);
      } else {
        throw new NotFoundException("File not found");
      }
    } else {
      throw new NotFoundException("Dataset not found");
    }
    ;
  }

  @Override
  public Dataset getDataset(UUID id) throws NotFoundException {
    final Optional<DatasetEntity> datasetEntity = datasetRepository.findById(id);
    if (datasetEntity.isPresent()) {
      return datasetMapper.toDtoDataset(datasetEntity.get());
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public void updateDataset(UUID id, Dataset dataset) throws NotFoundException {
    final Optional<DatasetEntity> datasetEntity = datasetRepository.findById(id);
    if (datasetEntity.isPresent()) {
      final DatasetEntity mappedEntity =
          datasetMapper.updateDatasetFromDto(datasetEntity.get(), dataset);
      datasetRepository.save(mappedEntity);
    } else {
      throw new NotFoundException("Dataset not found");
    }
  }

  @Override
  public Dataset addProductDataset(UUID id, Dataset dataset) throws NotFoundException {
    final Optional<ProductEntity> prodEntity = productRepository.findById(id);
    if (prodEntity.isPresent()) {
      DatasetEntity datasetEntity = datasetMapper.toEntity(dataset);
      datasetEntity.setProductEntity(prodEntity.get());
      datasetEntity = datasetRepository.save(datasetEntity);
      return datasetMapper.toDtoDataset(datasetEntity);
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  @Override
  public List<Dataset> getProductDatasets(UUID id) throws NotFoundException {
    final Optional<ProductEntity> prodEntity = productRepository.findById(id);
    if (prodEntity.isPresent()) {
      return datasetMapper.toListDto(prodEntity.get().getDatasetEntities());
    } else {
      throw new NotFoundException("Product not found");
    }
  }
}
