package com.epam.sberdex.product.config;

import com.epam.sberdex.camundalib.api.CustomTaskApi;
import io.swagger.client.ApiClient;
import io.swagger.client.api.ProcessDefinitionApi;
import io.swagger.client.api.ProcessInstanceApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CamundaApiConfig {

  @Value("${spring.application.services.camunda.path}")
  private String camundaPath;

  /**
   * Been camunda client.
   *
   * @return new ApiClient.
   */
  @Bean
  public ApiClient apiClient() {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(camundaPath);
    //    DateTimeFormatter formatter =
    //        DateTimeFormatter.ofPattern(CamundaConstant.CAMUNDA_DATE_TIME_PATTERN);
    //    apiClient.getJSON().setOffsetDateTimeFormat(formatter);
    return apiClient;
  }

  /**
   * Been Camunda ProcessDefinitionApi.
   *
   * @return new ProcessDefinitionApi.
   */
  @Bean
  public ProcessDefinitionApi processDefinitionApi() {
    return new ProcessDefinitionApi(apiClient());
  }

  /**
   * Been Camunda ProcessInstanceApi.
   *
   * @return new ProcessInstanceApi.
   */
  @Bean
  public ProcessInstanceApi processInstanceApi() {
    return new ProcessInstanceApi(apiClient());
  }

  /**
   * Been Camunda TaskApi.
   *
   * @return new TaskApi.
   */
  @Bean
  public CustomTaskApi taskApi() {
    return new CustomTaskApi(apiClient());
  }
}
