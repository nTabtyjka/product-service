package com.epam.sberdex.product.entity;

import com.epam.sberdex.common.db.entity.BaseEntity;
import com.epam.sberdex.product.model.Dataset;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "dataset")
public class DatasetEntity extends BaseEntity {
  private String name;

  @Enumerated(value = EnumType.STRING)
  private Dataset.StatusEnum status;

  @Enumerated(value = EnumType.STRING)
  private Dataset.DeliveryTypeEnum deliveryType;

  @ManyToOne
  @JoinColumn(name = "product_id")
  private ProductEntity productEntity;

  @OneToMany(mappedBy = "datasetEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private List<FileEntity> fileEntities;
}
