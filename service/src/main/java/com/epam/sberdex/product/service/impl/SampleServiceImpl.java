package com.epam.sberdex.product.service.impl;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.entity.SampleEntity;
import com.epam.sberdex.product.mapper.SampleMapper;
import com.epam.sberdex.product.model.Sample;
import com.epam.sberdex.product.repository.ProductRepository;
import com.epam.sberdex.product.repository.SampleRepository;
import com.epam.sberdex.product.service.FileTransferService;
import com.epam.sberdex.product.service.SampleService;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

@Service
@RequiredArgsConstructor
public class SampleServiceImpl implements SampleService {

  private final ProductRepository productRepository;
  private final ObjectFactory<ServletWebRequest> requestHolder;
  private final SampleRepository sampleRepository;
  private final FileTransferService storageService;
  private final SampleMapper sampleMapper;

  @Value("${spring.application.sample-root}")
  private String sampleRoot = "";

  @Override
  public List<Sample> addSample(UUID productId, MultipartFile file) throws NotFoundException {
    final Optional<ProductEntity> prod = productRepository.findById(productId);
    if (prod.isPresent()) {
      final StandardMultipartHttpServletRequest multipartRequest =
          new StandardMultipartHttpServletRequest(requestHolder.getObject().getRequest());
      final List<MultipartFile> files = multipartRequest.getFiles("file");
      final List<Sample> sampleList =
          files.stream()
              .map(
                  f -> {
                    final SampleEntity sampleEntity = new SampleEntity();
                    sampleEntity.setName(f.getOriginalFilename());
                    sampleEntity.setMimeType(f.getContentType());
                    sampleEntity.setSize(f.getSize());
                    sampleEntity.setProductEntity(prod.get());
                    final SampleEntity savedEntity = sampleRepository.save(sampleEntity);
                    final Path samplePath = getSampleFilePath(productId, savedEntity);
                    storageService.upload(samplePath, f);
                    return savedEntity;
                  })
              .map(sampleMapper::toDtoSample)
              .collect(Collectors.toList());
      return sampleList;
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  @Override
  public List<Sample> getSamples(UUID productId) throws NotFoundException {
    final Optional<ProductEntity> prod = productRepository.findById(productId);
    if (prod.isPresent()) {
      ProductEntity productEntity = prod.get();
      return sampleMapper.toDtoSamples(productEntity.getSampleEntities());
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  @Override
  public void deleteSample(UUID productId, UUID sampleId) throws NotFoundException {
    if (productRepository.existsById(productId) && sampleRepository.existsById(sampleId)) {
      SampleEntity sampleEntity = sampleRepository.findById(sampleId).orElse(null);
      if (sampleEntity != null) {
        storageService.delete(getSampleFilePath(productId, sampleEntity));
        sampleRepository.deleteById(sampleId);
      } else {
        throw new NotFoundException("Sample not found");
      }
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  @Override
  public Resource downloadSample(UUID productId, UUID sampleId) throws NotFoundException {
    if (productRepository.existsById(productId)) {
      final SampleEntity sampleEntity = sampleRepository.findById(sampleId).orElse(null);
      if (sampleEntity != null) {
        Resource resource = storageService.download(getSampleFilePath(productId, sampleEntity));
        if (resource.isReadable()) {
          return resource;
        } else {
          throw new RuntimeException("Resource not readable");
        }
      } else {
        throw new NotFoundException("Sample not found");
      }
    } else {
      throw new NotFoundException("Product not found");
    }
  }

  private Path getSampleFilePath(UUID productId, SampleEntity sampleEntity) {
    return Paths.get(
        sampleRoot,
        productId.toString(),
        sampleEntity.getId().toString() + " - " + sampleEntity.getName());
  }
}
