package com.epam.sberdex.product.service.camunda.impl;

import com.epam.sberdex.product.entity.DatasetEntity;
import com.epam.sberdex.product.entity.FileEntity;
import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.model.Product.StatusEnum;
import com.epam.sberdex.product.service.camunda.CamundaValidationService;
import com.epam.sberdex.product.util.exception.CamundaValidationException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@SuppressWarnings("all")
public class CamundaValidationServiceImpl implements CamundaValidationService {

  private static ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
  private static Validator validator = validatorFactory.getValidator();

  /**
   * Validation method for ProductEntity.
   *
   * @param productEntity productEntity
   * @throws CamundaValidationException CamundaValidationException
   */
  public boolean validate(@Valid ProductEntity productEntity) throws CamundaValidationException {
    if (StatusEnum.APPROVED.equals(productEntity.getStatus())) {
      return true;
    }
    return validateGeneric(productEntity);
  }

  @Override
  public boolean validateDatasetFileHash(@Valid ProductEntity productEntity)
      throws CamundaValidationException {
    StringBuilder stringBuilder = new StringBuilder();
    boolean result = true;
    final List<DatasetEntity> datasetEntities = productEntity.getDatasetEntities();
    if (datasetEntities != null) {
      for (DatasetEntity datasetEntity : datasetEntities) {
        if (datasetEntity.getFileEntities() != null) {
          for (FileEntity fileEntity : datasetEntity.getFileEntities()) {
            try {
              if (fileEntity.getHash() != null) {
                result &= validateGeneric(fileEntity);
              } else {
                throw new CamundaValidationException("Hash must be not null");
              }
            } catch (CamundaValidationException ex) {
              stringBuilder.append(fileEntity.getHash()).append(" ").append(ex.getMessage());
              log.error(ex.getMessage(), ex);
              result = false;
            }
          }
        }
      }
    }
    if (!result) {
      throw new CamundaValidationException(stringBuilder.toString());
    }
    return result;
  }

  private <T> boolean validateGeneric(@Valid T entity) throws CamundaValidationException {
    Set<ConstraintViolation<T>> constraintViolationSet = validator.validate(entity);
    if (!constraintViolationSet.isEmpty()) {
      Iterator<ConstraintViolation<T>> iterator = constraintViolationSet.iterator();
      StringBuilder stringBuilder = new StringBuilder();
      while (iterator.hasNext()) {
        stringBuilder.append(iterator.next().getMessage());
        stringBuilder.append(System.lineSeparator());
      }
      throw new CamundaValidationException(stringBuilder.toString());
    }
    return true;
  }
}
