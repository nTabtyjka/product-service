package com.epam.sberdex.product.service.impl;

import com.epam.sberdex.product.service.FileTransferService;
import com.epam.sberdex.product.util.FileTransferHelper;
import com.epam.sberdex.product.util.exception.FileTransferException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
@Slf4j
public class FileTransferServiceImpl implements FileTransferService {

  private final FileTransferHelper fileTransferHelper;

  @Override
  public Path upload(Path path, MultipartFile file) {
    final Path resolved = resolve(path);
    try {
      fileTransferHelper.createDirectory(resolved.getParent());
      fileTransferHelper.multipartFileTransfer(file, resolved);
    } catch (IOException e) {
      throw new FileTransferException("Cannot add file: " + resolved, e);
    }
    return resolved;
  }

  @Override
  public Resource download(Path path) {
    return fileTransferHelper.getResource(resolve(path));
  }

  @Override
  public void delete(Path path) {
    final Path resolvedPath = resolve(path);
    try {
      fileTransferHelper.delete(resolvedPath);
    } catch (NoSuchFileException e) {
      log.error("Error deleting file " + resolvedPath, e);
    } catch (IOException e) {
      throw new FileTransferException("Could not read file: " + resolvedPath, e);
    }
  }

  private Path resolve(Path path) {
    return fileTransferHelper.resolvePath(path);
  }
}
