package com.epam.sberdex.product.service;

import com.epam.sberdex.product.model.Product;
import com.epam.sberdex.product.model.ProductPage;
import java.util.UUID;
import javassist.NotFoundException;

public interface ProductService {
  ProductPage searchProducts(Integer pageNum, Integer pageSize, String status);

  Product addProduct(Product product);

  Product getProduct(UUID id) throws NotFoundException;

  void updateProduct(UUID id, Product product) throws NotFoundException;

  void deleteProduct(UUID id) throws NotFoundException;
}
