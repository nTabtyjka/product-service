package com.epam.sberdex.product.service.rest;

import com.epam.sberdex.product.api.DatasetApi;
import com.epam.sberdex.product.model.Dataset;
import com.epam.sberdex.product.model.FileDto;
import com.epam.sberdex.product.model.FileHashDto;
import com.epam.sberdex.product.service.DatasetService;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javassist.NotFoundException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DatasetApiImpl implements DatasetApi {

  private final DatasetService service;

  @Override
  public ResponseEntity<FileDto> addFile(UUID id, @Valid FileDto fileDto) {
    try {
      return ResponseEntity.ok(service.addFile(id, fileDto));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Void> addFileHash(UUID id, UUID fileId, @Valid FileHashDto fileHashDto) {
    try {
      service.addFileHash(id, fileId, fileHashDto);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Dataset> addProductDataset(UUID id, @Valid Dataset dataset) {
    try {
      return ResponseEntity.ok(service.addProductDataset(id, dataset));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<FileDto> addUploadFile(UUID id, MultipartFile file) {
    try {
      return ResponseEntity.ok(service.addUploadFile(id, file));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Void> deleteDataset(UUID id) {
    try {
      service.deleteDataset(id);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Void> deleteFile(UUID id, UUID fileId) {
    try {
      service.deleteFile(id, fileId);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Resource> downloadFile(UUID id, UUID fileId) {
    try {
      final Resource resource = service.downloadFile(id, fileId);
      final HttpHeaders headers = new HttpHeaders();
      headers.setContentDisposition(
          ContentDisposition.builder("attachment")
              .filename(resource.getFile().getName())
              .size(resource.getFile().length())
              .build());
      return ResponseEntity.status(HttpStatus.OK).headers(headers).body(resource);
    } catch (NotFoundException | IOException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Dataset> getDataset(UUID id) {
    try {
      return ResponseEntity.ok(service.getDataset(id));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<List<FileDto>> getFiles(UUID id) {
    try {
      return ResponseEntity.ok(service.getFiles(id));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<List<Dataset>> getProductDatasets(UUID id) {
    try {
      return ResponseEntity.ok(service.getProductDatasets(id));
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Void> updateDataset(UUID id, @Valid Dataset dataset) {
    try {
      service.updateDataset(id, dataset);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Void> updateFile(UUID id, UUID fileId, @Valid FileDto fileDto) {
    try {
      service.updateFile(id, fileId, fileDto);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<Void> uploadFile(UUID id, UUID fileId, MultipartFile file) {
    try {
      service.uploadFile(id, fileId, file);
      return ResponseEntity.status(HttpStatus.OK).build();
    } catch (NotFoundException e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.notFound().build();
    }
  }
}
