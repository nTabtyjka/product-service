package com.epam.sberdex.product.service.camunda;

import com.epam.sberdex.product.model.CompleteUserTaskDto;
import io.swagger.client.ApiException;
import io.swagger.client.model.VariableValueDto;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public interface CamundaService {
  UUID startBusinessProcess(String processKey) throws ApiException;

  void deleteBusinessProcess(UUID processInstanceId) throws ApiException;

  void setVariableToInstance(UUID instanceId, String varName, VariableValueDto variableValue)
      throws ApiException;

  void updateVariable(UUID instanceId, String varName, VariableValueDto variableValue)
      throws ApiException;

  void cancelCurrentUserTask(UUID instanceId) throws ApiException;

  void cancelCurrentUserTask(UUID instanceId, CompleteUserTaskDto dto) throws ApiException;
}
