package com.epam.sberdex.product.mapper;

import com.epam.sberdex.product.entity.FileEntity;
import com.epam.sberdex.product.model.FileDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface FileMapper {

  FileDto toDtoFile(FileEntity entity);

  FileEntity toEntityFile(FileDto productDto);

  List<FileDto> toDtoFiles(List<FileEntity> entities);

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "size", ignore = true)
  @Mapping(target = "uploadTime", ignore = true)
  @Mapping(target = "name", ignore = true)
  FileEntity updateFromDto(FileDto fileDto, @MappingTarget FileEntity entity);
}
