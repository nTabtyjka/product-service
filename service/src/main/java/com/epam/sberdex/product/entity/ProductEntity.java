package com.epam.sberdex.product.entity;

import com.epam.sberdex.common.db.entity.MultitenantEntity;
import com.epam.sberdex.product.model.Product;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@Entity(name = "product")
public class ProductEntity extends MultitenantEntity {

  public static final String FIELD_NAME = "name";
  public static final String FIELD_ID = "id";

  @NotBlank(message = "Name cannot be empty!")
  @Length(max = 256, message = "Name too long!")
  @Column(length = 256)
  private String name;

  @NotBlank(message = "Description cannot be empty!")
  @Length(max = 1024, message = "Description too long!")
  @Column(length = 1024)
  private String description;

  @NotNull(message = "Type cannot be empty!")
  @Enumerated(EnumType.STRING)
  private Product.TypeEnum type;

  @Enumerated(EnumType.STRING)
  private Product.StatusEnum status;

  @Column(name = "last_upload_at")
  private OffsetDateTime lastUploadAt;

  @Column(length = 512)
  private String rejectionReason;

  @Column(name = "instance_id")
  private UUID instanceId;

  @OneToMany(mappedBy = "productEntity")
  private List<SampleEntity> sampleEntities;

  @OneToMany(mappedBy = "productEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private List<DatasetEntity> datasetEntities;
}
