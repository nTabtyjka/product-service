package com.epam.sberdex.product.service;

import java.nio.file.Path;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileTransferService {
  Path upload(Path path, MultipartFile file);

  Resource download(Path path);

  void delete(Path path);
}
