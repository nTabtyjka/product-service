package com.epam.sberdex.product.mapper;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.model.Product;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface ProductMapper {

  Product toDtoProduct(ProductEntity entity);

  ProductEntity toEntityProduct(Product productDto);

  List<Product> toDtoProducts(List<ProductEntity> entities);

  @Mapping(target = "id", ignore = true)
  @Mapping(
      target = "type",
      nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  ProductEntity updateFromDto(Product productDto, @MappingTarget ProductEntity entity);
}
