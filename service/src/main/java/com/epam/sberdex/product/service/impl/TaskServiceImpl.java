package com.epam.sberdex.product.service.impl;

import com.epam.sberdex.product.entity.ProductEntity;
import com.epam.sberdex.product.model.CompleteUserTaskDto;
import com.epam.sberdex.product.repository.ProductRepository;
import com.epam.sberdex.product.service.TaskService;
import com.epam.sberdex.product.service.camunda.CamundaService;
import io.swagger.client.ApiException;
import java.util.UUID;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

  private final CamundaService camundaService;
  private final ProductRepository productRepository;

  @Override
  public void completeTask(UUID id, CompleteUserTaskDto dto) throws NotFoundException {
    ProductEntity currentProduct = productRepository.findById(id).orElse(null);
    if (currentProduct != null) {
      UUID currentInstanceId = currentProduct.getInstanceId();
      try {
        camundaService.cancelCurrentUserTask(currentInstanceId, dto);
      } catch (ApiException e) {
        log.error(e.getMessage(), e);
      }
    } else {
      throw new NotFoundException("Product not found");
    }
  }
}
