# Copyright 2018 EPAM Systems.

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

# See the License for the specific language governing permissions and
# limitations under the License.

apiVersion: v1
kind: Template
metadata:
  name: "sberdex-product-service"
  annotations:
    iconClass: "icon-spring"
    description: Openshift template for spring-boot multimodule application/service deploying
    template.openshift.io/provider-display-name: EPAM
    template.openshift.io/support-url: https://www.epam.com
objects:
  - apiVersion: v1
    kind: Service
    metadata:
      name: "sberdex-product-service"
      labels:
        app: "sberdex-product-service"
    spec:
      ports:
        - name: "sberdex-product-service"
          port: 8080
          protocol: TCP
          targetPort: 8080
      selector:
        app: "sberdex-product-service"
      type: ClusterIP
  - apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: "sberdex-product-service"

  - apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: 'sberdex-product-service-stage-data'
    spec:
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: '${STORAGE_CAPACITY}'
      storageClassName: 'efs'

  - apiVersion: v1
    data:
      stage_dir: /stage
    kind: ConfigMap
    metadata:
      name: sberdex-product-service-config

  - apiVersion: v1
    kind: Route
    metadata:
      name: "sberdex-product-service"
      annotations:
        description: "Route for sberdex-product-service application"
      labels:
        app: "sberdex-product-service"
    spec:
      tls:
        insecureEdgeTerminationPolicy: Redirect
        termination: edge

      host: product-service-${NAMESPACE}.demo.edp-epam.com

      path: /
      port:
        targetPort: 8080
      to:
        kind: Service
        name: "sberdex-product-service"
      wildcardPolicy: None


  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      name: "sberdex-product-service"
      labels:
        app: "sberdex-product-service"
    spec:
      replicas: 1
      selector:
        app: "sberdex-product-service"
      strategy:
        activeDeadlineSeconds: 21600
        type: Rolling
        rollingParams:
          failurePolicy: Abort
          intervalSeconds: 1
          maxSurge: 25%
          maxUnavailable: 25%
          timeoutSeconds: 600
          updatePeriodSeconds: 1
      template:
        metadata:
          labels:
            app: "sberdex-product-service"
        spec:

          serviceAccountName: "sberdex-product-service"
          containers:
            - name: "sberdex-product-service"
              image: "docker-registry.default.svc:5000/${IMAGE_NAME}:${APP_VERSION}"
              imagePullPolicy: Always
              env:
                - name: POSTGRES_USER
                  valueFrom:
                    secretKeyRef:
                      key: postgres.username
                      name: postgres
                - name: POSTGRES_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      key: postgres.password
                      name: postgres
                - name: STAGE_DIR
                  valueFrom:
                    configMapKeyRef:
                      name: sberdex-product-service-config
                      key: stage_dir
                - name: LANG
                  value: 'en_US.UTF-8'
              ports:
                - containerPort: 8080
                  protocol: TCP
              volumeMounts:
                - mountPath: /stage
                  name: "sberdex-product-service-stage-data"
              livenessProbe:
                failureThreshold: 5
                initialDelaySeconds: 180
                periodSeconds: 20
                successThreshold: 1
                tcpSocket:
                  port: 8080
                timeoutSeconds: 5
              readinessProbe:
                failureThreshold: 5
                initialDelaySeconds: 60
                periodSeconds: 20
                successThreshold: 1
                tcpSocket:
                  port: 8080
                timeoutSeconds: 5
              resources:
                requests:
                  memory: 500Mi

              terminationMessagePath: /dev/termination-log
              terminationMessagePolicy: File
          volumes:
            - name: "sberdex-product-service-stage-data"
              persistentVolumeClaim:
                claimName: 'sberdex-product-service-stage-data'
          dnsPolicy: ClusterFirst
          restartPolicy: Always
          schedulerName: default-scheduler
          securityContext: {}
          terminationGracePeriodSeconds: 30
      triggers:
        - type: ConfigChange
parameters:
  - displayName: Application image name
    name: IMAGE_NAME
    required: true
    value: "sberdex-product-service"
  - displayName: Application version
    name: APP_VERSION
    required: true
    value: "latest"
  - displayName: Current namespace
    name: NAMESPACE
    required: true
  - displayName: OSD storage capacity
    name: STORAGE_CAPACITY
    required: true
    value: 16Gi